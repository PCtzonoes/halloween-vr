using System;
using Helper;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class GameplayUIScreen : UIScreen
	{
		[SerializeField] private TMP_Text txt;

		public void ResetButton()
		{
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);

			UIScreenController.ShowScreen<GameplayUIScreen>();
		}

		public void ResetLevel(int nextLvl)
		{
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			SceneManager.LoadScene(nextLvl, LoadSceneMode.Single);
			UIScreenController.ShowScreen<GameplayUIScreen>();
		}

		public void ChangeTxt(string text)
		{
			txt.text = text;
		}
	}
}