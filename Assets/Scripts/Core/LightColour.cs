using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightColour : MonoBehaviour
{
	Light _light;

	private void Start()
	{
		_light = GetComponent<Light>();
	}

	public void GreenLight()
	{
		_light.color = Color.green;
	}
	
	public void BlueLight()
	{
		_light.color = Color.blue;
	}
	
	public void RedLight()
	{
		_light.color = Color.red;
	}
}
