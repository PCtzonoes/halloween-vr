using UnityEngine;

namespace Core
{
	public class Activator : MonoBehaviour
	{
		[SerializeField] protected GameObject targetGameObject;
		[SerializeField] private float waitTime = 3;
		
		private float _timer;
		
		private void OnEnable()
		{
			_timer = waitTime;
		}

		private void FixedUpdate()
		{
			_timer -= Time.fixedDeltaTime;
			if (_timer < 0)
			{
				targetGameObject.SetActive(true);
				// gameObject.SetActive(false);
			}
		}
	}
}