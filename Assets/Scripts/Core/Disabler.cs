using System;
using UnityEngine;

namespace Core
{
	public class Disabler : MonoBehaviour
	{
		[SerializeField] private float lifeTime = 3;
		
		private float _timer;
		
		private void OnEnable()
		{
			_timer = lifeTime;
		}

		private void FixedUpdate()
		{
			_timer -= Time.fixedDeltaTime;
			if (_timer < 0)
			{
				gameObject.SetActive(false);
			}
		}
	}
}