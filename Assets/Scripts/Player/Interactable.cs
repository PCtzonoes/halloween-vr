using System;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
	[SerializeField] private UnityEvent onInteract;

	protected virtual void Start()
	{
		if (onInteract == null)
		{
			Debug.LogError("Interactable missing events.", this);
			gameObject.SetActive(false);
		}
	}

	public void Interact()
	{
		onInteract.Invoke();
	}
}