using UnityEngine;

public class InteractionBehavior : MonoBehaviour
{
	[SerializeField] private LayerMask interactableLayer;
	[SerializeField] private Transform rayOrigin;

	public void Interact()
	{
		var faceRay = new Ray(rayOrigin.position, rayOrigin.forward);

		if (!Physics.Raycast(faceRay, out var hit, Mathf.Infinity, interactableLayer))
		{
			Debug.Log($"no hit on interact", this);
			return;
		}

		var interactable = hit.transform.GetComponent<Interactable>();
		if (interactable == null)
		{
			Debug.LogError($"item on interact layer should be an Interactable");
			return;
		}

		interactable.Interact();
	}
}