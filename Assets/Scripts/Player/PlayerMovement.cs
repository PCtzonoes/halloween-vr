using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;


[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField] private Transform playerCamera;
	[SerializeField] private CharacterController characterController;

	[Space] [Range(0.5f, 10f)] [SerializeField]
	private float moveSpeed = 1f;

	private Vector2 _moveInput;
	private Vector3 _playerMovement;

	private void Start()
	{
		if (playerCamera == null || characterController == null)
		{
			Debug.LogError("Player Missing Serialised fields.", this);
			gameObject.SetActive(false);
		}
	}

	private void Update()
	{
		_playerMovement = new Vector3(_moveInput.x, 0, _moveInput.y).CameraDirection(playerCamera).normalized;

		characterController.Move(_playerMovement * moveSpeed * Time.deltaTime);
	}

	public void Move(InputAction.CallbackContext ctx)
	{
		_moveInput = ctx.ReadValue<Vector2>();
	}
}